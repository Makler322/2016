import matplotlib.pyplot as plt

def function(x): # Функция y(x), по х возвращает у
    return x**2

def pr(x): # Функция возращающая производную от х**2
    return 2*x

def euler(x,y,step): #Алгоритм Эйлера
    x+=step
    y+=pr(x)*step
    return x,y

X,Y=[],[]

k=11 # Конечный х
step=0.001
for i in range(9):
    x=1 #Анулирование переменных
    y=1
    k-=1
    while x<k:
        x,y=euler(x, y, step)[0], euler(x, y, step)[1]
    okr=8  # Округление до okr знака
    X.append(k)  # Добавляю в конечный массив только конечную разность, а не текущую
    Y.append(round(round(abs(y-function(x)),okr)/function(x),okr))
graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('The final value of x')
plt.ylabel('Value')
ax.plot(X,Y)
print(X)
print(Y)
plt.show()