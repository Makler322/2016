def function(x): # Функция y(x), по х возвращает у
    return x**2

def pr(x): # Функция возращающая производную от х**2
    return 2*x

def eiler(x,y,k,step): #Алгоритм Эйлера
    okr=8 #Округление до okr знака
    while x<k:
        x+=step
        x=round(x,okr)
        y+=pr(x)*step
        y=round(y,okr)
        print(x,y,round(function(x),okr))

x=1
y=1
k=2
#step=0.1 # Выбираем нужным нам шаг
step=0.01
eiler(x,y,k,step)
