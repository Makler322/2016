from random import randint as random, gauss as gauss #В задаче потребуется генератор случайных чисел
import matplotlib.pyplot as plt #Выведем график, что-бы всё было по красоте

Pmin,Pmax=0,4 #Минимальное и максимальное кол-во клиентов вошедших за 1 минуту
Tmin,Tmax=1,9 #Минимальное и максимаьное время обслуживания каждого человека (по условию задачи в минутах)
L=480 #Достаточно большой интервал времени(по условию задачи в минутах)
M=15 #Допустимое время ожидания
procent=0.05 #Удволетворяющий нас процент "недовольных" посетителей
X=[]
Y=[]



def euler(K,N): #Функция Эйлера
    ploxo=0 #Вводим счётчик плохих исходов
    sigma=0.3 #Отклонение для функции Гаусса
    #P=random(Pmin,Pmax) #P-кол-во клиентов вошедших за данную минуту
    P=gauss((Pmin+Pmax)//2,((Pmin+Pmax)//2)*sigma)
    #T=random(Tmin,Tmax)
    T=gauss((Tmin+Tmax)//2,((Tmin+Tmax)//2)*sigma)
    R=K/T #R-кол-во клиентов обслуженных за это время
    if N+P-R<0: #Число клиентов находящихся в помещении, если оно меньше 0, то число приравниваем  0
        N=0
    else:
        N=N+P-R
    Q=N/K #Средняя длина очереди, где K-кол-во касс
    t=Q*T #Среднее время ожидания
    if t>M: #Счётчик плохих исходов
        ploxo+=1
    return ploxo,N
j=0
while 1!=0: #Основная программа подсчитывающая отоношение плохих исходов ко всем, в зависимости от касс
    j+=1
    ploxo=0 #Анулирование переменных
    N=0
    for i in range(L):
        N=euler(j,N)[1] #Начальное кол-во посетителей для каждой минуты
        ploxo+=euler(j,N)[0]
    X.append(j)
    Y.append((ploxo/L)*100)
    if ploxo/L<=procent: #Если данное кол-во касс нас удволетворяет, то выводим это значение и останавливаем программу
        print(j)
        break
graph = plt.figure() #Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Number of cashbox')
plt.ylabel('Percent of dissatisfied people')
ax.plot(X,Y)
plt.show()