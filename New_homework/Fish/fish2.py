import matplotlib.pyplot as plt

def euler(L,N0,Z0,D,k0,bn,bz): #Алгоритм Эйлера
    Z0=Z0-D*Z0+bz*N0*Z0 #Щуки
    N0=(1+((L-N0)/L)*k0)*N0-bn*N0*Z0 #Караси
    return Z0, N0

L=100 #Максимальная численность
N0=50 #Начальная численность карасей
Z0=10 #Начальная численность щук
D=0.8 #Коэффициент смертности щук
bn=0.01 #Коэффициент смертности карасей
bz=0.012 #Коэффициент прироста щук
k0=0.5 #Коэффициент прироста какрасей
Time=30 #Промежуток времени за который мы будем просматривать изменение популяции

X,Y,Y1=[0],[Z0],[N0] #Массив времени, массив изменения популяции щук, массив изменения популяции карасей

for i in range(1,Time+1):
    Z0,N0=euler(L,N0,Z0,D,k0,bn,bz)
    X.append(i) #Время
    Y.append(Z0) #Щуки
    Y1.append(N0) #Караси

graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Year')
plt.ylabel('Yellow-Pikes, Red-Crucians')
ax.plot(X,Y,'yellow')
ax.plot(X,Y1,'r')
plt.show()