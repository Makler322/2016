import matplotlib.pyplot as plt

def euler(N0,N1,N2,k0,L,R): #Алгоритм Эйлера
    N0=(1+(1-N0/L)*k0)*N0 #Алгоритм для каждой из формул
    N1=(1+(1-N1/L)*k0)*N1-R
    N2=(1+k0)*N2
    return N0, N1, N2

N0=100 #Начальная численность
R=40 #Учет отловленных рыб
k0=0.5 #Начальный коэффициент прироста (при нулевой числености)
L=1000 #Максимальная численность популяции
Time=8 #Промежуток времени за который мы будем просматривать изменение популяции
N1=N0 #Начальная численность для подсчёта моделью Ферхюльста с учётом отлова
N2=N0 #Начальная численность для подсчёта моделью Мальтуса
X,Y,Y1,Y2=[0],[N0],[N0],[N0]

for i in range(1,Time+1):
    N0,N1,N2=euler(N0,N1,N2,k0,L,R)
    X.append(i)
    Y.append(N0) #Результат для модели Ферхюльста
    Y1.append(N1) #Результат для модели Ферхюльста с учётом отлова
    Y2.append(N2) #Результат для модели Мальтуса



graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Year')
plt.ylabel('Y-Model of Verhulst, R-Verhulst with catching, G-Model of Malthus')
ax.plot(X,Y,'yellow')
ax.plot(X,Y1,'r')
ax.plot(X,Y2,'g')
plt.show()
