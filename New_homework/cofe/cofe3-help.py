import matplotlib.pyplot as plt

r=0.041 #Коэффициент r, посчитанный нами ранее

def eiler(n): #Алгоритм Эйлера

    Tnew=90
    Tk=22
    dT=1/600
    k=0
    while Tnew>=n:
        Tnew=Tnew-r*(Tnew-Tk)*dT
        k+=dT
    Tnew=Tnew-5
    l=k
    while Tnew>=75:
        Tnew=Tnew-r*(Tnew-Tk)*dT
        k+=dT
    return k,l
n=80
X=[]
Y=[]
Z=[]
for i in range(1000):
    n+=0.01
    X.append(n)
    Y.append(eiler(n)[0])
    Z.append(eiler(n)[1])
    #print(n, eiler(n))

graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
ax.plot(X,Y,'b')
#ax.plot(X,Z,'g')
#ax.plot(X2,Y2,'r')
print(X)
print(Y)
print(Z)
plt.show()