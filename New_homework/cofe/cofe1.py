import matplotlib.pyplot as plt

X=[] #Массив времени (координта х)
Y=[]
Yex=[83, 77.7, 75.1, 73, 71.1, 69.4, 67.8, 66.4, 64.7, 63.4, 62.1, 61, 59.9, 58.7, 57.8, 56.6] #Ручками вводим эксперементальные данные
Tstart=Yex[0]  #Начальная температура кофе
Tk=22 #Комнатная температура
dT=1 #Шаг времени
r=0 #Коэффициент r

def eiler(r,Tnew,i): #Алгоритм Эйлера
    Tnew=Tnew-r*(Tnew-Tk)*dT
    return (Yex[i]-Tnew)**2, Tnew

for i in range(1000):
    r+=0.001 #Прибавляем к коэффициенту шаг
    k=0
    Tstart=Yex[0] #Анулирование начальной переменной

    for j in range(1,len(Yex)): #Запускаем цикл в котором запускаем эйлера
        k+=eiler(r,Tstart,j)[0]
        Tstart=eiler(r,Tstart,j)[1]

    X.append(r) #После цикла добавляем r
    Y.append(k/(j-1)) #А так же результат программы после цикла

graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Coefficient r')
plt.ylabel('Standard deviation')
ax.plot(X,Y)
j=min(Y)
for i in range(len(Y)):
    if Y[i]==j:
        print(X[i])
        break #Выводим минимальное r
plt.show()