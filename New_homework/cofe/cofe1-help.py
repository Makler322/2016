import matplotlib.pyplot as plt
def function(x): # Функция y(x), по х возвращает у
    return x**2
X=[]
Y=[]
def pr(x): # Функция возращающая производную от х**2
    return 2*x

def eiler(r, Tn): #Алгоритм Эйлера
    S = [83, 77.7, 75.1, 73, 71.1, 69.4, 67.8, 66.4, 64.7, 63.4, 62.1, 61, 59.9, 58.7, 57.8, 56.6]
    Tk=22
    dT=1
    okr = 8
    k=0
    n=16
    for i in range(1,n):
        Tnew=Tn-r*(Tn-Tk)*dT
        k+=(S[i]-Tnew)**2
        #print(i, Tnew,X[i])
        Tn=Tnew
    X.append(r)
    Y.append(k/(i-1))
Tn=83
r=0
for i in range(1000):
    r+=0.001
    eiler(r,Tn)

graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
ax.plot(X,Y)
print(X)
print(Y)
j=min(Y)
for i in range(len(Y)):
    if Y[i]==j:
        print(X[i],Y[i])
        break
plt.show()