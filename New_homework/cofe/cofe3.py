import matplotlib.pyplot as plt


r=0.041 #Коэффициент r, посчитанный нами ранее
Tk=22 #Комнатная температура
Tstart=90  #Начальная температура кофе
dT=1/1000 #Шаг времени
Tend=75 #Конечная температура кофе
milk=5 #Температура на которую молоко охладит кофе
n=Tend+milk #Температура от которой мы будем добавлять молоко
X = [] #Массив температуры
Y = [] #Массив времени


def euler(Tnew): #Функция Эйлера
    Tnew=Tnew-r*(Tnew-Tk)*dT
    return Tnew


while n<=Tstart:
    Tnew=90  #Начальная температура кофе, анулирование каждый раз
    n+=0.01 #Шаг с которым идёт изменение температры
    k=0 #Итоговое время, с каждой температурой идёт анулирование
    while euler(Tnew)>=n: #Сначала идём Эйлером до теспературы n
        Tnew=euler(Tnew)
        k+=dT
    Tnew-=milk #Потом добавляем молоко
    while euler(Tnew)>=Tend: #Потом идём Элером до конечной температуры
        Tnew=euler(Tnew)
        k+=dT
    X.append(n) #Добавляем в массивы температуру и время
    Y.append(k)


graph = plt.figure()  #Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('The temperature at which added milk, C')
plt.ylabel('Cooling time, minutes')
ax.plot(X,Y, 'b')
plt.show()