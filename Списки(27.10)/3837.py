s=list(map(int, input().split()))
c=int(input())
for i in range(len(s)-1):
    if s[0]<c:
        print(1)
        break
    elif s[i]>=c and s[i+1]<c:
        print(i+2)
        break
    elif s[-1]>=c:
        print(len(s)+1)
        break
if len(s)==1:
    if s[0]>=c:
        print(2)
    else:
        print(1)