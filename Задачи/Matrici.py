DEBUG = False
def vvod(n): #К сожалению эту функцию нельзя проверить, т.к. на вход подаётя кол-во строчек,
    s=[]     #а на выход список строк из чисел которые ввёл пользователь
    for i in range (0,n):
        s.append(list(map(int, input().split())))
    return s
def peremnozenie(A,B,n,m,k): #На вход  требуется аж 5 параметров, это 2 матрицы, которые мы хоти перемножить, длина и ширина первой и второй, прчём ширина первой и длина второй должны совпадать.
    """
    >>> peremnozenie([[1, 2], [3, 4]],[[1, 0], [0, 1]],2,2,2)
    [[1, 2], [3, 4]]
    >>> peremnozenie([[1, 2]],[[3], [4]],1,2,1)
    [[11]]
    """
    s=[]
    for i in range(k):
        for o in range(n):
            c=0
            for u in range(m):
                c+=A[i][u]*B[u][o] #Мы складываем все эелементы m раз
            s.append(c) #Потом добавляем в новый список
        s.append('.') #Для удобства разделяем каким-то символом
    P=[[] for i in range(s.count('.'))] #Создаем "пустую" матрицу, считая кол-во элементов по разделительным знакам
    k=0
    for i in range(len(s)):
        if s[i]!='.':
            P[k].append(s[i])#Добавляем в каждую ячейку значения в данной строке
        else:
            k+=1 #Меняем строку
    return P #На выходе получаем нужную матрицу
if __name__=='__main__': #Как-же без ДокТестов-то? ^_^
    if DEBUG:
        import doctest
        doctest.testmod()
    else:
        n,m=map(int,input().split())
        A=vvod(n)
        m,k=map(int,input().split())
        B=vvod(m)
        C=peremnozenie(A,B,n,m,k)
        for i in range(len(C)): #Из полученной матрицы выводим числа в том формате в котором нам нужно
            print(' '.join(map(str,C[i])))