def far(list_a,val):
    ai = 0 # указывает на начало массива
    bi = len(list_a) - 1 # указывает на конец массива
    while ai <= bi:
        ki = (bi - ai + 1) // 2 + ai
        if list_a[ki] < val:
            ai = ki + 1
        elif list_a[ki] == val:
            break
        else:
            bi = ki - 1

    if list_a[ki] > val:
        return ki
    elif list_a[ki] != val:
        return ki + 1
    else:
        return ki

def lin_search(list_a,val):
    k=0
    while list_a[k]!=val:
        k+=1
    return k

from random import randint, choice
from time import time
for n in range(1000, 10000, 1000):
    list_a = [randint(-100000,100000) for j in range(n)]
    list_a.sort()
    vals = [choice(list_a) for i in range(1000)]
    t1 = time()
    for val in vals:
        res = far(list_a, val)
    t2 = time()
    print("{n}; {time: .4f}".format(n=n, time=time() - t1), end =";")
    t1=time()
    for val in vals:
        key=lin_search(list_a,val)
    print("{n};{time:.2f}".format(n=n,time=time()-t1))

