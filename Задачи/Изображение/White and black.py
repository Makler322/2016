from PIL import Image #Импортируем библиотеку PIL
img=Image.open ('Photo.jpg').convert('RGB') #Берём и открываем фото
h,l=img.size #Высота и длина
for i in range(h): #Пробегаемся по высоте и длине
    for j in range(l):
        RGB=img.getpixel((i,j))
        r=RGB[0] #Узнаем параметры красного, зеленого  и голубого
        g=RGB[1]
        b=RGB[2]
        img.putpixel((i,j),((r+g+b)//3,(r+g+b)//3,(r+g+b)//3)) #Данному пикселю задаю среднее арифметическое значение всех трех параметров
img.save('New_Photo.jpg') #Сохраняем
img.show() #Показываем