from PIL import Image #Импортируем библиотеку PIL
img=Image.open ('Photo.jpg').convert('RGB') #Берём и открываем фото
h,l=img.size #Высота и длина
for i in range(h): #Пробегаемся по высоте и длине
    for j in range(l):
        RGB=img.getpixel((i,j))
        r=(RGB[0]*100)//256+100 #Меняем параметры
        g=(RGB[1]*100)//256+100
        b=(RGB[2]*100)//256+100
        img.putpixel((i,j),(r,g,b)) #Данному пикселю задаю значение всех трех параметров
img.save('New_Photo.jpg') #Сохраняем
img.show() #Показываем