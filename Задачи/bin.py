def fa(a, l): #Функция бинарного поиска
    ai = 0
    bi = len(a) - 1
    while ai <= bi:
        k = (bi - ai + 1) // 2 + ai
        if a[k] > l:
            bi = k - 1
        elif a[k] == l:
            return l #Если мы нашли нужный нам элемент, то он выводится, а если его нет, то ничего не выводится :(
        elif a[k] < l:
            ai = k + 1