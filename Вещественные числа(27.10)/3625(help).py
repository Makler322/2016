import math
def cos(x, eps):
    x %= 2 * math.pi
    s = 0
    sl = 1
    psl = 0
    z = 0
    while abs(sl - psl) >= eps:
        s += sl
        psl = sl
        sl *= -x * x / ((z + 1) * (z + 2))
        z += 2
    return s
x=int(input())
eps=float(input())
print(cos(x,eps))