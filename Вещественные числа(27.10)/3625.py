

def leeeel(num):
    try:
        b = float(num)
    except OverflowError:
        b = 10**50

    return b

n = int(input())
x0 = float(input())

summ = 1.0

one = -1

fact = 2

x = x0 * x0


for i in range(2, n + 2):
    summ += (one) * (x) / leeeel(fact)

    one *= -1

    fact *= (2 * i - 1)
    fact *= (2 * i)

    x *= x0
    x *= x0

print(summ)