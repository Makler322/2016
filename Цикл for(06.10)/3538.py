DEBUG = False
def fa(a,b):
    """
    >>> fa(1,10)
    2 4 6 8 10
    """
    n=[]
    for i in range((a+1)//2,(b+2)//2):
        i=i*2
        n.append(i)
    return n
if __name__ == "__main__":
    if DEBUG:
        import doctest
        doctest.testmod()
    else:
        a=int(input())
        b=int(input())
        print (' '.join(map(str, fa(a,b))))