a=int(input())
b=int(input())
c=int(input())
if a+b>c and a+c>b and c+b>a:
    s=a+b+c
    a=max(a,b,c)
    c=min(a,b,c)
    b=s-a-c
    if a**2==b**2+c**2:
        print("rectangular")
    elif a**2<b**2+c**2:
        print("acute")
    else:
        print("obtuse")
else:
    print("impossible")