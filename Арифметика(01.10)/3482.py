DEBUG = False
def fa(a):
    """
    >>> fa(2)
    1
    """

if __name__ == "__main__":
    if DEBUG:
        import doctest
        doctest.testmod()
    else:
        a=int(input())
        print(fa(a))